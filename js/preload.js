// IIF

(function (){
    
    let loading = 0;
    let id = setInterval(frame,8);
    let preload = document.getElementById('preload'); 

    function frame(){
        if(loading == 100){
            clearInterval(id);
            preload.style.animation = "fadeout 1s ease";
            setTimeout(function(){
                preload.style.display = "none";
            }, 1000)
        } else {
            loading = loading + 1;
        }
    }
})();

