// Initialization
var marginY = 0;
var destination = 0;
var speed = 10;
var scroller = null;
//  CONST
const TIME = 2;
const TOP = 0;


// Scroll to specific id element
function initScroll(elementId){
	destination = document.getElementById(elementId).offsetTop;
	
	scroller = setTimeout(function(){initScroll(elementId);}, TIME);
	marginY = marginY + speed;

    // Check to stop recursion
	if(marginY >= destination - 10){
		clearTimeout(scroller);
	}	

    // Scroll
	window.scroll(0, marginY);
}

window.onscroll = function(){
	marginY = this.pageYOffset;	
};


// Scroll to top
function toTop(){
    
    scroller = setTimeout(function(){toTop();}, TIME);
	marginY = marginY - speed;

    // Check to stop recursion
	if(marginY <= TOP){
		clearTimeout(scroller);
	}	

    // Scroll    
	window.scroll(0, marginY);
}