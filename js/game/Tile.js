
class Tile {
    constructor(x, y, color = DEFAULTCOLOR, visible = false) {
      this.x = x;
      this.y = y;
      this.beggingX = x;
      this.beggingY = y;
      this.color = color;
      this.visible = visible;
    }

    mayMove(x,y){
        let futureX = this.x + x;
        let futureY = this.y + y;

        let checkWidth = (futureX > 0 && futureX < WIDTH-1); 
        let checkHeight = (futureY >= 0 && futureY < HEIGHT-1)
        let gridFree = !(gridik.tileGrid[futureX][futureY].visible);

        return( checkWidth && checkHeight && gridFree);
     }

     moveTile(x,y){
        this.x += x;
        this.y += y;
     }

     draw(){
        stroke(0);
        strokeWeight(1);
        fill(this.color);
        // width, heigt
        rect(this.x * BOX,this.y * BOX, BOX, BOX);
     }

}