// 
// TETRIS GAME
//

// Basic const initialization
const BOX = 32;
const SPEED = 4;
const WIDTH = 19;
const HEIGHT = 19;
const REDCOLOR = "Red";
const DEFAULTCOLOR = "Black";
const NEGATIVEONE = -1;
const MAXROWINHIGHSCORE = 5;

const SHAPE = ["O","I", "S", "Z", "L", "J", "T"];
const COLORS = ["Red","Blue", "Yellow", "White", "Purple", "Pink"];

// Basic variable initialization
let direciton = "DOWN"; // default

// Set up images
const ground = new Image();
ground.src = "../img/ground.png";

// Shapes ( Omega, Ironman, Superman, Zombie, Lucy in the Sky, Jan Hrdy, Thor )