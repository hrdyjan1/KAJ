var actualBlock;
var gridik;
// Game
var continuePLaying = true;
var score = 0;
// Switch menu
var showMenu = true;
var showHighScore = false;
// Buttons
var startButton;
var highScoreButton;

var table;

// p5js
function preload(){
    // img = loadImage(bgImg);

    // Start Game Button
    startButton = createButton("Start");
    startButton.parent("game-menu");
    startButton.mousePressed(() => {
        
        // Show game
        showMenu = false;
        showHighScore = false; 
        startButton.hide();
        highScoreButton.hide();
        continuePLaying = true;

        document.getElementById('table').style.visibility = "hidden";
        if(document.getElementById('highscore') !== null) {
            document.getElementById('highscore').remove();
        }
    });



    // HighScore Button
    highScoreButton = createButton("HightScore");
    highScoreButton.parent("game-menu");
    highScoreButton.mousePressed(() => {

        // Table
        table = new p5.Table();
        table.addColumn('rank');
        table.addColumn('score');
        
        // Switch menu
        showMenu = false;
        showHighScore = true;  

        // Hide buttons
        // startButton.hide();
        highScoreButton.hide();  

        // Actulize table - use localStorage
        let index = 1;
        Object.keys(localStorage).forEach(function(key){
            let newRow = table.addRow();
            newRow.setNum('rank', index);
            newRow.setString('score', JSON.parse(localStorage.getItem("top" + index)).score);
            index++;
        });        

        makeHighScoreTable(table.getRowCount());
        document.getElementById('table').style.visibility = "visible";
    });
}

// p5js
function setup() {

    // Canvas
    let canvas = createCanvas(WIDTH * BOX, HEIGHT * BOX);
    canvas.parent('game');

    // Game initialization
    makeNewGridAndActualBlock();    

    // Speed of falling blocks
    frameRate(SPEED);
} 
  
// p5js
function draw() {
    
    if(showMenu === true)
    {

        // Basic logic
        clear();
        background(50,50,50);
        textSize(64);
        
        // Tetris text
        fill(249,243,244);
        textFont("Open Sans");
        textAlign(CENTER);
        text("Tetris", (WIDTH * BOX)/2, (WIDTH * BOX)/2);
        textAlign(CENTER);

        
    } else if (showHighScore === true){
        clear();
        background(50,50,50);
        textSize(64);

        
        fill(249,243,244);
        textFont("Open Sans");

    } else  {


        // Clear and draw;
        clear();

        fill(255);
        textSize(32);        
        text("Score " + score, (WIDTH * BOX)/2, 35);

        // Check stopping game
        if(!continuePLaying){
            stopTheGame();
            return;
        } 
        
        // Draw Block and move the ActualBlock
        actualBlock.draw();
        let stillActualBlock = actualBlock.update();

        // Check if need new block
        if(!stillActualBlock){

            // Add to grid
            for (let i = 0; i < actualBlock.tiles.length; i++) {
                let whatX = actualBlock.tiles[i].x;
                let whatY = actualBlock.tiles[i].y;
                gridik.tileGrid[whatX][whatY].visible = true;
                gridik.tileGrid[whatX][whatY].color = "Green";
            }

            // Check if line complete
            // This piece of code must be here ( NOT REMOVE )
            score += gridik.solveLineComplete(actualBlock);

            const randomShapeNumber = Math.floor(Math.random() * SHAPE.length)  
            const randomColorNumber = Math.floor(Math.random() * COLORS.length)  
            actualBlock = new Block(SHAPE[randomShapeNumber], COLORS[randomColorNumber]);
            // actualBlock = new Block(SHAPE[2], COLORS[randomColorNumber]);     
            
            continuePLaying = actualBlock.moveToRightPossition();

        }

        // Draw grid 
        gridik.drawGrid();
    }

}

// p5js
function keyPressed() {

    if (showMenu) return

    switch (keyCode) {
  
        case LEFT_ARROW:
            // noLoop();
            actualBlock.move(-1, 0);
            break;
    
        case RIGHT_ARROW:
            actualBlock.move(1, 0);
            // loop();
            break;
    
        case UP_ARROW:
            actualBlock.rotate();
            // actualBlock.rotate(1);
            break;

        // Start - button S
        case 83:
            loop();
            break;

        // Pause - button P
        case 80:
            noLoop();
            break;
        
        // Stop The game - button Space 
        case 32:
            stopTheGame();
            break;
        
    }
}

function makeNewGridAndActualBlock(){

    // Grid
    gridik = new Grid();
    gridik.initGrid();
    
    // actualBlock
    const randomShapeNumber = Math.floor(Math.random() * SHAPE.length);
    const randomColorNumber = Math.floor(Math.random() * COLORS.length);
    
    actualBlock = new Block(SHAPE[randomShapeNumber], COLORS[randomColorNumber]);
    actualBlock.moveToRightPossition();

    score = 0;
}

function stopTheGame(){
    
    // Show menu 
    showMenu = true;
    startButton.show();
    highScoreButton.show();
    continuePLaying = false;
    
    let index = 0;
    Object.keys(localStorage).forEach(function(key){
        let itemFromLocalStorage = localStorage.getItem(key);
        let scoreTMP = JSON.parse(itemFromLocalStorage).score;
        if(scoreTMP < score){
            let newScore = {"score": score.toString()};
            let newScoreStringify = JSON.stringify(newScore);
            localStorage.setItem(key,newScoreStringify);
            console.log("scoreTMP" + scoreTMP + " < score" + score);
            score = scoreTMP;
        }
        index++;
    });

    if(index < MAXROWINHIGHSCORE) {
        index++;
        
        let newScore = {"score": score.toString()};
        let newScoreStringify = JSON.stringify(newScore);
        localStorage.setItem("top" + index,newScoreStringify);
    }


    // Game initialization
    makeNewGridAndActualBlock();
}

function makeHighScoreTable(rowCount){
    
    let aTable = document.createElement('table');
    aTable.id = "highscore";

    // Heading
    let tr1 = document.createElement('tr');
    let th1 = document.createElement('th');        
    let th2 = document.createElement('th');        

    th1.appendChild(document.createTextNode(table.columns[0]))
    th2.appendChild(document.createTextNode(table.columns[1]))

    tr1.appendChild(th1);        
    tr1.appendChild(th2);        
    aTable.appendChild(tr1);
    
    // Body
    for (let i = 0, tr, td; i < rowCount; i++) {
        tr = document.createElement('tr');
        let row = table.getRow(i);
        
        for (var c = 0; c < table.getColumnCount(); c++) {
            td = document.createElement('td');        
            td.appendChild(document.createTextNode(row.getString(c)));
            tr.appendChild(td);
        }
        aTable.appendChild(tr);
    }
    
    //update with id
    document.getElementById('table').appendChild(aTable);

}
