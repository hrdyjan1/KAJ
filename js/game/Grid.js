
function Grid() {
    this.tileGrid = [];
}

Grid.prototype.initGrid = function (){

    for (let i = 0; i < (WIDTH); i++) {
        this.tileGrid[i] = []; // assembling 2D array
        for (let j = 0; j < (HEIGHT); j++) {
            this.tileGrid[i][j] = new Tile(i, j); // empty tile
        }
    }
};

Grid.prototype.drawGrid = function(){
    for (let j = 0; j < gridik.tileGrid.length; j++) {
        for (let k = 0; k < gridik.tileGrid[j].length; k++) {
            if(gridik.tileGrid[j][k].visible){
                
                const  XPossition = gridik.tileGrid[j][k].x;
                const YPosstioon = gridik.tileGrid[j][k].y
                
                stroke(0);
                strokeWeight(1);
                fill("Green");
                rect( XPossition * BOX, YPosstioon * BOX, BOX, BOX);
            }
        }
    }
}

Grid.prototype.solveLineComplete = function(block){

    let countOfDeletedLines = 0; 
    let arrayOfCompletedLines = [];

    // Check if any line is completed
    for (let i = 0; i < block.tiles.length; i++) {
        let ans = this.checkLineCompleted(block.tiles[i]);
        arrayOfCompletedLines.push(ans);
    }

    // Only unique items
    arrayOfCompletedLines = arrayOfCompletedLines.sort().filter(function(el,i,a){if(i==a.indexOf(el))return 1;return 0})

    console.log(arrayOfCompletedLines);
    
    // Delete lines
    for (let k = 0; k < arrayOfCompletedLines.length; k++) {
        const value = arrayOfCompletedLines[k];

        if(value !== NEGATIVEONE ){
            
            // Hide line
            for (let k = 1; k < gridik.tileGrid.length-1; k++) {
                gridik.tileGrid[k][value].visible = false;
            }

            this.changeVisiblePieces(value);
            countOfDeletedLines++;
        }
    }

    return countOfDeletedLines;
}

Grid.prototype.checkLineCompleted = function(tile){

    for (let k = 1; k < gridik.tileGrid.length-1; k++) {
        if(!(gridik.tileGrid[k][tile.y].visible)){
            return -1;
        }
    }

    return tile.y;
}

Grid.prototype.changeVisiblePieces = function(possitonY) {

    for( let j = 0; j < gridik.tileGrid.length; j++) {
        for (let k = possitonY-1; k > 0; k--) {
            if(gridik.tileGrid[j][k].visible){

                gridik.tileGrid[j][k].visible = false;                    
                gridik.tileGrid[j][k+1].visible = true;
            
            } 
        }
    }

     
}