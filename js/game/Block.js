

class Block {

    constructor(shape, color = "Black"){
        this.shape = shape;
        this.tiles = this.makeBlockOfTiles(shape,color);
    }

    makeBlockOfTiles(shape, color = "Black"){

        let tilesTMP = [];
        
        // Shapes ( Omega, Ironman, Superman, Zombie, Lucy in the Sky, Jan Hrdy, Thor )
        switch(shape) {
            //Omega
            case SHAPE[0]:
                tilesTMP.push(new Tile(1,1,color));
                tilesTMP.push(new Tile(2,1,color));
                tilesTMP.push(new Tile(1,2,color));
                tilesTMP.push(new Tile(2,2,color));
                break;
            //Ironman
            case SHAPE[1]:
                tilesTMP.push(new Tile(0,1,color));
                tilesTMP.push(new Tile(1,1,color));
                tilesTMP.push(new Tile(2,1,color));
                tilesTMP.push(new Tile(3,1,color));
                break;
            //Superman
            case SHAPE[2]:
                tilesTMP.push(new Tile(1,1,color));
                tilesTMP.push(new Tile(2,1,color));
                tilesTMP.push(new Tile(2,2,color));
                tilesTMP.push(new Tile(3,2,color));
                break;
            //Zombie
            case SHAPE[3]:
                tilesTMP.push(new Tile(2,1,color));
                tilesTMP.push(new Tile(3,1,color));
                tilesTMP.push(new Tile(1,2,color));
                tilesTMP.push(new Tile(2,2,color));
                break;
            //Lucy in the Sky
            case SHAPE[4]:
                tilesTMP.push(new Tile(1,1,color));
                tilesTMP.push(new Tile(2,1,color));
                tilesTMP.push(new Tile(1,2,color));
                tilesTMP.push(new Tile(1,3,color));
                break;
            //Jan Hrdy
            case SHAPE[5]:
                tilesTMP.push(new Tile(1,1,color));
                tilesTMP.push(new Tile(2,1,color));
                tilesTMP.push(new Tile(2,2,color));
                tilesTMP.push(new Tile(2,3,color));
                break;
            //Thor
            case SHAPE[6]:
                tilesTMP.push(new Tile(1,1,color));
                tilesTMP.push(new Tile(2,1,color));
                tilesTMP.push(new Tile(3,1,color));
                tilesTMP.push(new Tile(2,2,color));
                break;
        }
        return tilesTMP;

    }

    draw(){
        for (let i = 0; i < this.tiles.length; i++) {
            this.tiles[i].draw();
        }
    }

    update(){
        return this.move(0,1);
    }

    // Move
    move(x,y){
        
        // Check if may Move
        for (let i = 0; i < this.tiles.length; i++) {
            if(!this.tiles[i].mayMove(x,y)){
                return false;
            }
        }

        // Move tiles
        for (let j = 0; j < this.tiles.length; j++) {
            this.tiles[j].moveTile(x,y);
        }
        return true;
    }

    // Move 
    moveToRightPossition(){
        return this.move(7,2);
    }

    rotate(){

        // Check if can rotate
        for(let j = 0; j < this.tiles.length; j++){
            
            let currentX = this.tiles[j].beggingX;
            let currentY = this.tiles[j].beggingY;
            
            let currentNumber = currentX + ( 4 * currentY);
            
            let newNumber = ( ((currentNumber + 1) * 4 ) - ((Math.floor(currentNumber/4)) + 1) ) % 16;

            let newCurrentX = newNumber % 4;
            let newCurrentY = Math.floor(newNumber/4);

            if(!this.tiles[j].mayMove(newCurrentX - currentX,newCurrentY - currentY)){
                return;
            }
        }

        // Make rotation
        for (let i = 0; i < this.tiles.length; i++) {

            let currentX = this.tiles[i].beggingX;
            let currentY = this.tiles[i].beggingY;
            
            let currentNumber = currentX + ( 4 * currentY);
            
            let newNumber = ( ((currentNumber + 1) * 4 ) - ((Math.floor(currentNumber/4)) + 1) ) % 16;

            let newCurrentX = newNumber % 4;
            let newCurrentY = Math.floor(newNumber/4);

            this.tiles[i].x = this.tiles[i].x + newCurrentX - currentX;
            this.tiles[i].y = this.tiles[i].y + newCurrentY - currentY;

            this.tiles[i].beggingX = newCurrentX;
            this.tiles[i].beggingY = newCurrentY;

        }
    }
}