// Basic initialization
const tetrisCanvas = document.getElementById("tetris");
const tetrisContext = tetrisCanvas.getContext("2d");

const box = 32;
let score = 0;
let direciton = "DOWN";

// Set up images
const ground = new Image();
ground.src = "../img/ground.png";

// create the tetrisBox
let tetrisBox = getRandomTetrisBox();

// Direction
document.addEventListener("keydown",direction);
function direction(event){
    let key = event.keyCode;
    if( key == 37){
       direction = "LEFT";
    }else if(key == 39){
       direction = "RIGHT";
    }else if(key == 40){
       direction = "DOWN";
    }
}

// draw everything to the canvas
function draw(){
    
    tetrisContext.drawImage(ground,0,0);
    
    for( let i = 0; i < tetrisBox.length ; i++){
        tetrisContext.fillStyle = "green";
        tetrisContext.fillRect(tetrisBox[i].x,tetrisBox[i].y,box,box);
    }
    
    // old head position
    let tetrisX = tetrisBox[0].x;
    let tetrisY = tetrisBox[0].y;
    
    // which direction
    if( direction == "LEFT" && tetrisX > box) {
        tetrisX -= box;
    } else if ( direction == "RIGHT" && tetrisX < 17 * box) {
        tetrisX += box;        
    } else {
        tetrisY += box;
    }

    direction = "DOWN";
    
    // remove the tail
    tetrisBox.pop();
    
    // add new Head
    let newHead = {
        x : tetrisX,
        y : tetrisY
    }
    
    // Check to make new tetrisBox
    if( tetrisY > 17*box ){
        clearInterval(game);
    }
    
    tetris.unshift(newHead);
    
    tetrisContext.fillStyle = "white";
    tetrisContext.font = "45px Changa one";
    tetrisContext.fillText(score,2*box,1.6*box);
}

// call draw function every 100 ms
let game = setInterval(draw,300);